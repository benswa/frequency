# Benson Liu
# bliu13@ucsc.edu
#
# CMPS 5P, Spring 2014
# Assignment 6
#
# This program takes in text files as input and counts the words
# and letters in the book. It will display the top 30 words used.

__author__ = 'bliu13'


from string import punctuation, ascii_lowercase


def delete_punctuation(s):
    """
    This function removes all the punctuation from the string.
    It will replace the punctuation with a space to prevent problems
    such as bliu13@ucsc.edu from being merged into bliu13ucscedu
    """
    new_s = s
    for p in '!"#$%&()*+,-./:;<=>?@[\]^_`{|}~':
        new_s = new_s.replace(p, " ")

    if punctuation[6] in new_s:
        new_s = new_s.replace(punctuation[6], "")
    return new_s


def process_book(book_file):
    """
    This function reads in a book and returns a tuple with two dictionaries:
    one with the count of all words (not just the top thirty) and another with
    the letter count. If your dictionaries are called word_count and letter_count,
    you can return them both with return word_count, letter_count, and the caller
    can get them with:

    words, letters = process_book ('jekyll_and_hyde.txt')
    """

    # Create dictionaries for word count and letter count
    word_count = dict()
    letter_count = dict()

    # Opening the file whose filename is the parameter
    text_document = None
    try:
        text_document = open(book_file, 'r')
    except IOError:
        print('Error: Filename "{0}" does not exist in the same directory as this program.'.format(book_file))
        exit(-1)

    for line in text_document:
        # Formats the line by deleting the newline character, splitting line into
        # a list of words and various other things.
        formatted_line = line.lower()
        formatted_line = delete_punctuation(formatted_line)
        formatted_line = formatted_line.replace("\n", "")
        line_list = formatted_line.split(' ')

        # Populating the word_count dictionary and letter_count dictionary
        # one word at a time in the line.
        for word_index in range(len(line_list)):
            if line_list[word_index] is not '':
                # Breaks the word into letters.
                letter_list = list(line_list[word_index])

                # Iterates through the list of letters to see if the word has
                # some kind of trash in there. This is for those odd cases
                # where some special characters were not caught, especially
                # if they have '\'. Word is ignored if oddities are found.
                is_a_word = 1
                for letter_index in range(len(letter_list)):
                    if letter_list[letter_index] not in ascii_lowercase:
                        is_a_word = 0
                if is_a_word is 0:
                    continue

                # Adds letter into dictionary if it does not exist and
                # increases letter count if the letter exists in the
                # dictionary. letter_list also contains trash characters,
                # but they are sorted out.
                for letter_index in range(len(letter_list)):
                    if letter_list[letter_index] not in letter_count:
                        letter_count[letter_list[letter_index]] = 1
                    elif letter_list[letter_index] in letter_count:
                        letter_count[letter_list[letter_index]] += 1

                # Adds the word into dictionary if it does not exist and
                # increases word count if the word exists in dictionary.
                if line_list[word_index] not in word_count:
                    word_count[line_list[word_index]] = 1
                elif line_list[word_index] in word_count:
                    word_count[line_list[word_index]] += 1
    text_document.close()
    return word_count, letter_count


def find_total_count(dictionary):
    """
    Takes in a dictionary and will iterate through the dictionary to sum up how many
    letters or values there are in the dictionary.
    """

    # Extracts the values in the dictionary
    total_count = 0
    for key in dictionary:
        total_count += dictionary[key]
    return total_count


def top_thirty(word_count):
    """
    This function takes a dictionary word_count and returns a list of the top thirty
    words from the dictionary, by count. If there's a tie for 30th place, the
    top-30 list may include any of the tied words; no need to pick any particular
    tied word.

    idea of sorting by value came from:
    http://www.pythoncentral.io/how-to-sort-python-dictionaries-by-key-or-value/
    """

    # Obtain the highest 30 value count.
    sorted_values = sorted(word_count.values())
    sorted_values.reverse()
    highest_thirty_values = sorted_values[:30]

    # Search through the dictionary to find which words have those those values
    # Words ordered from greatest to least.
    highest_thirty_keys = []
    for value in highest_thirty_values:
        for key in word_count:
            if word_count[key] is value:
                highest_thirty_keys.append(key)
    return highest_thirty_keys


def top_letters(letter_count):
    """
    This function is similar to top_thirty() but specifically used only for letters.
    It takes a dictionary letter_count and returns a list in order by count.

    idea of sorting by value came from:
    http://www.pythoncentral.io/how-to-sort-python-dictionaries-by-key-or-value/
    """

    # Order the dictionary of letters by decreasing value
    sorted_values = sorted(letter_count.values())
    sorted_values.reverse()

    # Search through the dictionary to find which letters have those those values
    # Letters ordered from greatest to least.
    ordered_letters = []
    for value in sorted_values:
        for key in letter_count:
            if letter_count[key] is value:
                ordered_letters.append(key)
    return ordered_letters


def find_top_frequency(dictionary, top_list, total_count):
    """
    This function is meant to be used with the input: list being the output of
    top_thirty(dictionary) and find_total_count(dictionary) of the same dictionary.

    The output is a dictionary of floats. Basically, the dictionary passed
    in are a bunch of keys (words or letters) and the values are the count.
    This function will return a dictionary with keys of the top thirty keys and
    its frequencies. The dictionary returned may not be in the same order
    as top_list.

    Parameter: dictionary is an actual dictionary
               top_list is a list of values that are keys that should exist in dictionary
               total_count are the sum of values in the dictionary
    """
    top_thirty_frequency = dict()
    for key in top_list:
        word_frequency = float(dictionary[key]) / float(total_count)
        top_thirty_frequency[key] = word_frequency
    return top_thirty_frequency


def find_frequency(dictionary, total_count):
    """
    This function is meant to be used with the input: the dictionary and
    find_total_count(dictionary) of the same dictionary.

    The output is a dictionary of floats. Basically, the dictionary passed
    in are a bunch of keys (words or letters) and the values are the count.
    This function will return the same dictionary with keys but the values
    are replaced with frequencies instead.

    Parameter: dictionary is an actual dictionary
               total_count are the sum of values in the dictionary
    """
    dictionary_frequency = dict()
    for key in dictionary:
        word_frequency = float(dictionary[key]) / float(total_count)
        dictionary_frequency[key] = word_frequency
    return dictionary_frequency


def merge_dictionaries(first_dictionary, second_dictionary):
    """
    This function will copy the first dictionary and then it will iterate through
    the second dictionary and then take the key value and iterate through the first
    dictionary to see if that entry exists. If it does, it will add the key's value
    in the second dictionary and adds it to the duplicate dictionary and when it is
    done, it will return the copied dictionary.
    """
    new_dictionary = first_dictionary.copy()
    for key in second_dictionary:
        if key in new_dictionary:
            new_dictionary[key] += second_dictionary[key]
        elif key not in new_dictionary:
            new_dictionary[key] = second_dictionary[key]
    return new_dictionary


if __name__ == '__main__':
    output = open('output.txt', 'w+')
    output.write('Frequency of Top Words and Letters\n\n')

    ### First Book ###
    output.write('1) The War of the Worlds by H.G. Wells\n')
    words1, letters1 = process_book('wells.txt')

    output.write('*** Frequency of the Top Thirty Words ***\n')
    top_thirty_in_words1 = top_thirty(words1)
    total_in_words1 = find_total_count(words1)
    top_thirty_freq_in_words1 = find_top_frequency(words1, top_thirty_in_words1, total_in_words1)
    for index in range(len(top_thirty_in_words1)):
        top_key = top_thirty_in_words1[index]
        output.write('{0}: {1}\n'.format(top_key, top_thirty_freq_in_words1[top_key]))
    output.write('\n')

    output.write('*** Frequency of the Letters ***\n')
    top_letters1 = top_letters(letters1)
    total_in_letters1 = find_total_count(letters1)
    top_thirty_freq_in_letters1 = find_top_frequency(letters1, top_letters1, total_in_letters1)
    for index in range(len(top_letters1)):
        top_key = top_letters1[index]
        output.write('{0}: {1}\n'.format(top_key, top_thirty_freq_in_letters1[top_key]))
    output.write('\n')

    ### Second Book ###
    output.write('2) Great Expectations by Charles Dickens\n')
    words2, letters2 = process_book('dickens.txt')

    output.write('*** Frequency of the Top Thirty Words ***\n')
    top_thirty_in_words2 = top_thirty(words2)
    total_in_words2 = find_total_count(words2)
    top_thirty_freq_in_words2 = find_top_frequency(words2, top_thirty_in_words2, total_in_words2)
    for index in range(len(top_thirty_in_words2)):
        top_key = top_thirty_in_words2[index]
        output.write('{0}: {1}\n'.format(top_key, top_thirty_freq_in_words2[top_key]))
    output.write('\n')

    output.write('*** Frequency of the Letters ***\n')
    top_letters2 = top_letters(letters2)
    total_in_letters2 = find_total_count(letters2)
    top_thirty_freq_in_letters2 = find_top_frequency(letters2, top_letters2, total_in_letters2)
    for index in range(len(top_letters2)):
        top_key = top_letters2[index]
        output.write('{0}: {1}\n'.format(top_key, top_thirty_freq_in_letters2[top_key]))
    output.write('\n')

    ### Third Book ###
    output.write('3) The Strange Case of Dr. Jekyll and Mr. Hyde by Robert Louis Stevenson\n')
    words3, letters3 = process_book('stevenson.txt')

    output.write('*** Frequency of the Top Thirty Words ***\n')
    top_thirty_in_words3 = top_thirty(words3)
    total_in_words3 = find_total_count(words3)
    top_thirty_freq_in_words3 = find_top_frequency(words3, top_thirty_in_words3, total_in_words3)
    for index in range(len(top_thirty_in_words3)):
        top_key = top_thirty_in_words3[index]
        output.write('{0}: {1}\n'.format(top_key, top_thirty_freq_in_words3[top_key]))
    output.write('\n')

    output.write('*** Frequency of the Letters ***\n')
    top_letters3 = top_letters(letters3)
    total_in_letters3 = find_total_count(letters3)
    top_thirty_freq_in_letters3 = find_top_frequency(letters3, top_letters3, total_in_letters3)
    for index in range(len(top_letters3)):
        top_key = top_letters3[index]
        output.write('{0}: {1}\n'.format(top_key, top_thirty_freq_in_letters3[top_key]))
    output.write('\n')

    # Frequencies of Top Words Across the Three Books
    # Starts by finding the frequencies of all words in the dictionaries
    # then merge the dictionaries to add together their frequencies.
    # The highest values of the key will tell us which words are used the most
    # across all three books. The average would be the division by 3 of those
    # values.
    output.write('*** Frequency of the Top Thirty Words Across the Three Books ***\n')
    word1_freq = find_frequency(words1, total_in_words1)
    word2_freq = find_frequency(words2, total_in_words2)
    word3_freq = find_frequency(words3, total_in_words3)

    words_freq_1_2 = merge_dictionaries(word1_freq, word2_freq)
    words_freq_1_2_3 = merge_dictionaries(words_freq_1_2, word3_freq)
    top_thirty_across = top_thirty(words_freq_1_2_3)
    for index in range(len(top_thirty_across)):
        top_key = top_thirty_across[index]
        output.write('{0}: {1}\n'.format(top_key, words_freq_1_2_3[top_key]))
    output.write('\n')

    output.close()
